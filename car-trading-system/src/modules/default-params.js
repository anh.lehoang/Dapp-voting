module.exports = {
    defaultConfigurationFile: './config.json',
    defaultGas: 5000000,
    defaultGasPrice: 20000000000,
};
