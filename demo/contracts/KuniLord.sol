pragma solidity ^0.4.16;


contract KuniLord {
    uint private constant MIN_PRICE = 100;
    address public owner;

    struct Lord {
        address addr;
        bytes32 nickname;
        uint totalSpending;
    }

    struct Kuni {
        bytes32 name;
        Lord lord;
        uint currentBuyPrice;
    }

    mapping (address => Lord) lords;
    mapping (bytes32 => Kuni) kunies;
    
    Kuni[] public occupiedKunies;
    Lord[] public occupingLords;


    constructor() public {
        owner = msg.sender;
    }

    function setNickname(bytes32 _nickname) public payable {
        lords[msg.sender].addr = msg.sender;
        lords[msg.sender].nickname = _nickname;
        emit SetNickname(msg.sender, _nickname);
    }

    function getNickname() public view returns (bytes32) {
      return lords[msg.sender].nickname;
    }   

    function checkKuniExist(bytes32 _name) internal view returns(bool){
      for (uint i = 0; i< occupiedKunies.length; i++) {
        if (occupiedKunies[i].name == _name) {
            return true;
        }
      }
      return false;
    }

    function getKuniInfo(bytes32 kuni) public view 
    returns(bytes32 name, address lordAddr, bytes32 lordName, uint currentBuyPrice) {
      require(kuni != "");
      if (checkKuniExist(kuni)) {
        return (kuni, 
              kunies[kuni].lord.addr, 
              kunies[kuni].lord.nickname, 
              kunies[kuni].currentBuyPrice);
      }
      return (kuni, 0, 0, kunies[kuni].currentBuyPrice);
    }

    function checkLordExist(address lordAddr) internal view returns(bool) {
      for(uint i = 0; i < occupingLords.length; i++) {
        if (occupingLords[i].addr == lordAddr) {
            return true;
        }
      }
      return false; 
    }

    function occupy(bytes32 nation) public payable {
        require(nation != "");
        if (checkKuniExist(nation)) {
            require(msg.value > kunies[nation].currentBuyPrice);
            
            kunies[nation].currentBuyPrice = msg.value;
            kunies[nation].lord = lords[msg.sender];
        }else {
            require(msg.value > INIT_BUY_PRICE);
            
            Kuni memory newKuni = Kuni({
                name: nation,
                currentBuyPrice: msg.value,
                lord: lords[msg.sender]
            });
            
            kunies[nation] = newKuni;
            occupiedKunies.push(newKuni);
        }
        lords[msg.sender].totalSpending += msg.value;
        msg.sender.transfer(msg.value);
        if (!checkLordExist(lords[msg.sender].addr)) {
            occupingLords.push(lords[msg.sender]);
        }
        emit OccupyKuni(msg.sender, nation, msg.value);
    }
}
